/*
 * Copyright 2012 Red Hat, Inc. and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
//Save as
package org.optaplanner.examples.vehiclerouting.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import java.util.HashMap;
import java.util.Map;
import org.optaplanner.core.api.domain.entity.PlanningEntity;
import org.optaplanner.core.api.domain.variable.CustomShadowVariable;

import org.optaplanner.examples.common.domain.AbstractPersistable;
import org.optaplanner.examples.vehiclerouting.domain.location.Location;
import org.optaplanner.examples.vehiclerouting.domain.timewindowed.solver.DummyDirtyUpdatingVariableListener;

@PlanningEntity
@XStreamAlias("VrpVehicle")
public class Vehicle extends AbstractPersistable implements Standstill {

    protected int capacity;
    protected int capacityh;
    protected Depot depot;
    protected int maxDuration;
    protected boolean roundtrip;
    protected boolean hasSchoolsInRoute;
    protected boolean multiSchool;
    protected String type;
    protected int countAsMe;
    private Long dummyDirty;
    private long dummySS;
    protected Map<Integer, Integer> currentCapacity = new HashMap<Integer, Integer>();

    // Shadow variables
    protected Customer nextCustomer;

    @CustomShadowVariable(variableListenerClass = DummyDirtyUpdatingVariableListener.class,
            // Arguable, to adhere to API specs (although this works), nextCustomer should also be a source,
            // because this shadow must be triggered after nextCustomer (but there is no need to be triggered by nextCustomer)
            sources = {
                @CustomShadowVariable.Source(variableName = "nextCustomer")})
//        @PlanningVariable(valueRangeProviderRefs = { "vehicleRange" })
    public Long getDummyDirty() {
        return dummyDirty;
    }

    public void setDummyDirty(Long dummyDirty) {
        this.dummyDirty = dummyDirty;
    }

    public long getDummySS() {
        return dummySS;
    }

    public void setDummySS(long dummySS) {
        this.dummySS = dummySS;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public String getType() {
        this.type = this.capacity + "-" + this.capacityh;
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getCountAsMe() {
        return countAsMe;
    }

    public void setCountAsMe(int countAsMe) {
        this.countAsMe = countAsMe;
    }

    public int getCapacityh() {
        return capacityh;
    }

    public void setCapacityh(int capacityh) {
        this.capacityh = capacityh;
    }

    public int getMaxDuration() {
        return maxDuration;
    }

    public void setMaxDuration(int maxDuration) {
        this.maxDuration = maxDuration;
    }

    public boolean isMultiSchool() {
        return multiSchool;
    }

    public void setMultiSchool(boolean multiSchool) {
        this.multiSchool = multiSchool;
    }

    public boolean getRoundtrip() {
        return roundtrip;
    }

    public void setRoundtrip(boolean roundtrip) {
        this.roundtrip = roundtrip;
    }

    public Depot getDepot() {
        return depot;
    }

    public void setDepot(Depot depot) {
        this.depot = depot;
    }

    public Customer getNextCustomer() {
        return nextCustomer;
    }

    // updated
    public void setNextCustomer(Customer nextCustomer) {
        this.nextCustomer = nextCustomer;
    }

    // ************************************************************************
    // Complex methods
    // ************************************************************************
    public boolean isHasSchoolsInRoute() {
        return hasSchoolsInRoute;
    }

    public void setHasSchoolsInRoute(boolean hasSchoolsInRoute) {
        this.hasSchoolsInRoute = hasSchoolsInRoute;
    }

    public Vehicle getVehicle() {
        return this;
    }

    public Location getLocation() {
        return depot.getLocation();
    }

    /**
     * @param standstill never null
     * @return a positive number, the distance multiplied by 1000 to avoid
     * floating point arithmetic rounding errors
     */
    public long getDistanceTo(Standstill standstill) {
        return depot.getDistanceTo(standstill);
    }

    @Override
    public String toString() {
        Location location = getLocation();
        if (location.getName() == null) {
            return super.toString();
        }
        return location.getName() + "/" + super.toString();
    }

    // Ubicabus
    public long getDistanceMetric() {
        long distanceTotal = 0;
        long distanceAccumulated = 0;

        if (nextCustomer != null) {
            Customer nextOne = nextCustomer;
            distanceTotal += nextOne.getDistanceFromPreviousStandstill();
            distanceAccumulated += distanceTotal;

            while (nextOne.getNextCustomer() != null) {
                nextOne = nextOne.getNextCustomer();
                distanceTotal += nextOne.getDistanceFromPreviousStandstill();
                distanceAccumulated += distanceTotal;
            }
        }

        return distanceAccumulated / 1000;
    }

    public boolean vdummy(int ac) {

        return true;
    }

    public String whosInMe() {
        String response = "";
        Customer cust = nextCustomer;
        if (cust != null) {
            response += "vehicle(" + this.getId() + ","+this.getType()+")";
            do {
                response += " ,id:" + cust.getId()+"("+cust.demand+","+cust.demandh+")";
                cust = cust.getNextCustomer();
            } while (cust != null);
        }
        return response;
    }

    public int getCapacityToHere(Customer customer) {
        int response = 0;
        Map<Integer, Integer> students = new HashMap<Integer, Integer>();
        // get first customer
        if (customer.getVehicle().getNextCustomer() != null) {
            Customer cust = customer.getVehicle().getNextCustomer();
            if (cust != null) {
                do {
                    // if student and belongs to school, add demand
                    if (cust.getId() == customer.getId()) {
                        break;
                    }
                    if (cust.isSchool()){ //.getType().equals("SCHOOL")) {
                        for (Map.Entry entry : cust.getStudents().entrySet()) {
                            students.put((Integer) entry.getKey(), 0);
                        }
                    } else {
                        for (Map.Entry entry : cust.getStudents().entrySet()) {
                            if (students.get((Integer) entry.getKey()) != null) {
                                students.put((Integer) entry.getKey(),
                                        students.get((Integer) entry.getKey()) + (Integer) entry.getValue());
                            } else {
                                students.put((Integer) entry.getKey(), (Integer) entry.getValue());
                            }
                        }
                    }
                    cust = cust.getNextCustomer();
                } while (cust != null);
                for (Map.Entry entry : customer.getStudents().entrySet()) {
                    if (students.get((Integer) entry.getKey()) != null) {
                        response += students.get((Integer) entry.getKey());
                    }
                }
            }
        }
        return response;
    }

    public long vehicleRouteScore() {
        long score = 0;
        Customer cust = this.getNextCustomer();
        if (cust != null) {
            do {
                score += this.getCapacityToHere(cust);
                cust = cust.getNextCustomer();
            } while (cust != null);
        }
        return -score;
    }

    public Integer routeScore() {//int ac) {

        Map<Integer, Integer> schools;
        Map<Integer, Integer> students;

        Customer cust = this.getNextCustomer();
        if (cust != null) {
            schools = new HashMap<Integer, Integer>();
            students = new HashMap<Integer, Integer>();
            cust = this.getNextCustomer();
            Integer response = 0;
            if (cust != null) {
                do {
                    if (cust.isSchool()){ //.getType().equals("SCHOOL")) {
                        for (Map.Entry entry : cust.getStudents().entrySet()) {
                            if (students.get((Integer) entry.getKey()) != null) {
                                schools.put((Integer) entry.getKey(), 0);
                            }
                        }
                    } else {
                        for (Map.Entry entry : cust.getStudents().entrySet()) {
                            students.put((Integer) entry.getKey(), 0);
                        }
                    }
                    cust = cust.getNextCustomer();

                } while (cust != null);
                response = students.size() - schools.size();
            }
            //*System.out.println("\n\n\nEvaluating route validity\n"+whosInMe()+" -->"+response+"<---\n\n\n");
            return Math.abs(response);
        } else {
            //*System.out.println("\n\n\nEvaluating route validity\n"+whosInMe()+" -->0<---\n\n\n");
            return 0;
        }
    }

   
    public Integer validMultiSchool() {
        //System.out.println("Valid Multischoool f() vehicel " + this.getId());
        if (this.getNextCustomer() != null && isHasSchoolsInRoute() == true && isMultiSchool() == false) {
            // get first customer of route
            Customer cust = getNextCustomer();
            Map<Integer, Integer> schools = new HashMap<Integer, Integer>();
            Integer max = 0;
            Integer schoolid = 0;
            do {
                if (!cust.isSchool()){ //cust.getType().equals("STUDENTS")) {
                    //dummy for because only 1 item since no cluster should have from multiple schools
                    for (Map.Entry entry : cust.getStudents().entrySet()) {
                        if (schools.get((Integer) entry.getKey()) != null) {
                            schools.put((Integer) entry.getKey(), schools.get((Integer) entry.getKey()) + 1);
                        } else {
                            schools.put((Integer) entry.getKey(), 1);
                        }
                        if (max > schools.get((Integer) entry.getKey())) {
                            max = schools.get((Integer) entry.getKey());
                            schoolid = (Integer) entry.getKey();
                        }
                    }
                }

                cust = cust.getNextCustomer();
            } while (cust != null);

            if (schools.size() > 1) {
                Integer accumulate = 0;
                for (Map.Entry entry : schools.entrySet()) {
                    if (max != (Integer) entry.getValue() || entry.getKey() != schoolid) {
                        accumulate += (Integer) entry.getValue();
                    }
                }
                return accumulate * 10000;
            }
        }

        return 0;
    }

    
    /**
     * isSchoolValid() V2
     * @return 
     */

    public Integer isSchoolValid() { // newGMS
        // get first customer of route
        Customer cust = this.getNextCustomer();
        
        Integer schoolBreak = 0;
        Integer customerBreak = 0;
        boolean schoolFlag=false;
        
        Map<Integer, Integer> schools = new HashMap<Integer, Integer>();
        if (cust != null) {
            do {
                if (!cust.isSchool()){ //cust.getType().equals("STUDENTS")) {
                    //STUDENTS
                    if (schoolFlag == true) {
                        // if a School already passed
                        schoolBreak += 1;
                    }
                } else {
                    //SCHOOLS
                    schoolFlag = true;

                    for (Map.Entry entry : cust.getStudents().entrySet()) {
                        if (schools.get((Integer) entry.getKey()) == null) {
                            schools.put((Integer) entry.getKey(), 0);
                        } else {
                            customerBreak += 1;
                        }
                    }
                }
                
                cust = cust.getNextCustomer();
                
            } while (cust != null);
        }
        return (customerBreak * 1000) + (schoolBreak * 10000);
    }
    
    
    
    
    
    
    
    
    public long distanceFromPreviousStandstill() {
        Customer cust = this.getNextCustomer();
        if (cust == null) {
            return 0;
        }

        long distanceTotal = 0;
        Customer lastValid = null;
        Map<Integer, Integer> students = new HashMap<Integer, Integer>();
        do {
            if (!cust.isSchool()){ //cust.getType().equals("STUDENTS")) {
                //is student
                for (Map.Entry entry : cust.getStudents().entrySet()) {
                    students.put((Integer) entry.getKey(), 0);
                }
                distanceTotal += cust.getDistanceFromPreviousStandstill();
                lastValid = cust;
                //System.out.println("total ("+distanceTotal+") to depot ("+this.getDepot().getDistanceTo(cust)+")");
            } else {
                //is school
                for (Map.Entry entry : cust.getStudents().entrySet()) {
                    //check school has students
                    if (students.get((Integer) entry.getKey()) != null) {
                        distanceTotal += cust.getDistanceFrom(lastValid);
                        lastValid = cust;
                    }
                }
            }
            if (cust.getNextCustomer() == null && this.roundtrip == true) {
                distanceTotal += this.getDepot().getDistanceTo(cust);
                System.out.println("total ("+distanceTotal+") to depot ("+this.getDepot().getDistanceTo(cust)+")");
            }
            cust = cust.getNextCustomer();
        } while (cust != null);
        return distanceTotal;
    }

}

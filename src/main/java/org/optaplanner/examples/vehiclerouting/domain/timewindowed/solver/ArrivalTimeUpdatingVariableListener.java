/*
 * Copyright 2015 Red Hat, Inc. and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.optaplanner.examples.vehiclerouting.domain.timewindowed.solver;

import org.apache.commons.lang3.ObjectUtils;
import org.optaplanner.core.impl.domain.variable.listener.VariableListener;
import org.optaplanner.core.impl.score.director.ScoreDirector;
import org.optaplanner.examples.vehiclerouting.domain.Customer;
import org.optaplanner.examples.vehiclerouting.domain.Standstill;
import org.optaplanner.examples.vehiclerouting.domain.Vehicle;
import org.optaplanner.examples.vehiclerouting.domain.timewindowed.TimeWindowedCustomer;

// TODO When this class is added only for TimeWindowedCustomer, use TimeWindowedCustomer instead of Customer
public class ArrivalTimeUpdatingVariableListener implements VariableListener<Standstill> {

    public void beforeEntityAdded(ScoreDirector scoreDirector, Standstill customer) {
        // Do nothing
    }

    public void afterEntityAdded(ScoreDirector scoreDirector, Standstill customer) {
        if (customer instanceof TimeWindowedCustomer) {
            updateArrivalTime(scoreDirector, (TimeWindowedCustomer) customer);
        }
    }

    public void beforeVariableChanged(ScoreDirector scoreDirector, Standstill customer) {
        // Do nothing
    }

    public void afterVariableChanged(ScoreDirector scoreDirector, Standstill customer) {
        if (customer instanceof TimeWindowedCustomer) {
            updateArrivalTime(scoreDirector, (TimeWindowedCustomer) customer);
        }
    }

    public void beforeEntityRemoved(ScoreDirector scoreDirector, Standstill customer) {
        // Do nothing
    }

    public void afterEntityRemoved(ScoreDirector scoreDirector, Standstill customer) {
        // Do nothing
    }

    protected void updateArrivalTime(ScoreDirector scoreDirector, TimeWindowedCustomer sourceCustomer) {
        //Standstill previousStandstill = sourceCustomer.getPreviousStandstill();
        //Long departureTime = (previousStandstill instanceof TimeWindowedCustomer)
        //        ? ((TimeWindowedCustomer) previousStandstill).getDepartureTime() : null;
        
        //Long arrivalTime = calculateArrivalTime(sourceCustomer, departureTime);
        scoreDirector.beforeVariableChanged(sourceCustomer, "arrivalTime");
        //sourceCustomer.setArrivalTime(arrivalTime);
        scoreDirector.afterVariableChanged(sourceCustomer, "arrivalTime");
        
        //System.out.println("lister start " + sourceCustomer.getId());
        
        if (sourceCustomer.getVehicle() != null) {
            TimeWindowedCustomer shadowCustomer = (TimeWindowedCustomer) sourceCustomer.getVehicle().getNextCustomer();
            //arrivalTime = calculateArrivalTime(shadowCustomer, departureTime);
            Vehicle myVehicle=sourceCustomer.getVehicle();
        //    myVehicle.setDummySS(0);
//   scoreDirector.beforeVariableChanged(myVehicle, "dummyDirty");
 //  scoreDirector.triggerVariableListeners();
   scoreDirector.afterVariableChanged(myVehicle, "dummyDirty");   
  //myVehicle.setp
  
       //scoreDirector.beforeVariableChanged(shadowCustomer, "dummyDirtys");
  
  
            while (shadowCustomer != null) {
               // System.out.println("lister " + shadowCustomer.getId());
                scoreDirector.beforeVariableChanged(shadowCustomer, "arrivalTime");
                //shadowCustomer.setArrivalTime(arrivalTime);
                scoreDirector.afterVariableChanged(shadowCustomer, "arrivalTime");
                //departureTime = shadowCustomer.getDepartureTime();
                shadowCustomer = shadowCustomer.getNextCustomer();
                //arrivalTime = calculateArrivalTime(shadowCustomer, departureTime);
            } 
       
        }
        //else
        //   System.out.println("lister no vehicle " + sourceCustomer.getId()); 
    }

    private Long calculateArrivalTime(TimeWindowedCustomer customer, Long previousDepartureTime) {
        if (customer == null || customer.getPreviousStandstill() == null) {
            return null;
        }
        if (previousDepartureTime == null) {
            // PreviousStandstill is the Vehicle, so we leave from the Depot at the best suitable time
            return Math.max(customer.getReadyTime(), customer.getDistanceFromPreviousStandstill());
        }
        return previousDepartureTime + customer.getDistanceFromPreviousStandstill();
    }

}

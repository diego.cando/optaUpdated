/*
 * Copyright 2012 Red Hat, Inc. and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.optaplanner.examples.vehiclerouting.domain;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import com.thoughtworks.xstream.annotations.XStreamInclude;

//import opta.planner.JsonVehicleRoute;
import org.optaplanner.core.api.domain.solution.PlanningEntityCollectionProperty;
import org.optaplanner.core.api.domain.solution.PlanningSolution;
import org.optaplanner.core.api.domain.solution.Solution;
import org.optaplanner.core.api.domain.valuerange.ValueRangeProvider;
import org.optaplanner.core.api.score.buildin.bendablelong.BendableLongScore;
import org.optaplanner.core.api.score.buildin.hardsoftlong.HardSoftLongScore;
import org.optaplanner.core.impl.score.buildin.bendablelong.BendableLongScoreDefinition;
import org.optaplanner.core.impl.score.buildin.hardsoftlong.HardSoftLongScoreDefinition;
import org.optaplanner.examples.common.domain.AbstractPersistable;
import org.optaplanner.examples.vehiclerouting.domain.location.DistanceType;
import org.optaplanner.examples.vehiclerouting.domain.location.Location;
import org.optaplanner.examples.vehiclerouting.domain.timewindowed.TimeWindowedVehicleRoutingSolution;
import org.optaplanner.persistence.xstream.impl.score.XStreamScoreConverter;

@PlanningSolution
@XStreamAlias("VrpVehicleRoutingSolution")
@XStreamInclude({TimeWindowedVehicleRoutingSolution.class})
public class JsonVehicleRoutingSolution extends AbstractPersistable implements Solution<BendableLongScore> {// HardSoftLongScore>
    // {

    protected String name;
//    protected DistanceType distanceType;
    //protected String distanceUnitOfMeasurement;
    //protected List<Location> locationList;
//    protected List<Depot> depotList;
    protected List<JsonVehicleRoute> vehicleRouteList;

    protected List<JsonCustomer> customerList;

    protected Boolean feasible;
    protected long hard0;
    protected long soft0;
    protected long soft1;
    protected long soft2;
    protected long soft3;

    // @XStreamConverter(value = XStreamScoreConverter.class, types = {HardSoftLongScoreDefinition.class})
    // protected HardSoftLongScore score;
    @XStreamConverter(value = XStreamScoreConverter.class, types = {BendableLongScoreDefinition.class}, ints = {1, 4})
    protected BendableLongScore score;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

// public DistanceType getDistanceType() {
//        return distanceType;
//    }
//
//    public void setDistanceType(DistanceType distanceType) {
//        this.distanceType = distanceType;
//    }
//
//    public String getDistanceUnitOfMeasurement() {
//        return distanceUnitOfMeasurement;
//    }
//
//    public void setDistanceUnitOfMeasurement(String distanceUnitOfMeasurement) {
//        this.distanceUnitOfMeasurement = distanceUnitOfMeasurement;
//    }
//
//    public List<Location> getLocationList() {
//        return locationList;
//    }
//
//    public void setLocationList(List<Location> locationList) {
//        this.locationList = locationList;
//    }
//
//    public List<Depot> getDepotList() {
//        return depotList;
//    }
//
//    public void setDepotList(List<Depot> depotList) {
//        this.depotList = depotList;
//    }
    @PlanningEntityCollectionProperty
    @ValueRangeProvider(id = "vehicleRange")
    public List<JsonVehicleRoute> getVehicleList() {
        return vehicleRouteList;
    }

    public void setVehicleList(List<JsonVehicleRoute> vehicleRouteList) {
        this.vehicleRouteList = vehicleRouteList;
    }

    @PlanningEntityCollectionProperty
    @ValueRangeProvider(id = "customerRange")
    public List<JsonCustomer> getCustomerList() {
        return customerList;
    }

    public void setCustomerList(List<JsonCustomer> customerList) {
        this.customerList = customerList;
    }

//    public HardSoftLongScore getScore() {
//        return score;
//    }
//
//    public void setScore(HardSoftLongScore score) {
//        this.score = score;
//    }
    public Boolean getFeasible() {
        return feasible;
    }

    public void setFeasible(Boolean feasible) {
        this.feasible = feasible;
    }

    public long getHard0() {
        return hard0;
    }

    public void setHard0(long hard0) {
        this.hard0 = hard0;
    }

    public long getSoft0() {
        return soft0;
    }

    public void setSoft0(long soft0) {
        this.soft0 = soft0;
    }

    public long getSoft1() {
        return soft1;
    }

    public void setSoft1(long soft1) {
        this.soft1 = soft1;
    }

    public long getSoft2() {
        return soft2;
    }

    public void setSoft2(long soft2) {
        this.soft2 = soft2;
    }

    public long getSoft3() {
        return soft3;
    }

    public void setSoft3(long soft3) {
        this.soft3 = soft3;
    }

    // ************************************************************************
    // Complex methods
    // ************************************************************************
//    public Collection<? extends Object> getProblemFacts() {
//        List<Object> facts = new ArrayList<Object>();
//        facts.addAll(locationList);
//        facts.addAll(depotList);
//        // Do not add the planning entities (vehicleList, customerList) because
//        // that will be done automatically
//        return facts;
//    }
    public BendableLongScore getScore() {
        return score;
    }

    public void setScore(BendableLongScore score) {
        this.score = score;
    }


//    public String getDistanceString(NumberFormat numberFormat) {
//        if (score == null) {
//            return null;
//        }
//        long distance = -score.getSoftScore(0);
//        if (distanceUnitOfMeasurement == null) {
//            return numberFormat.format(((double) distance) / 1000.0);
//        }
//        if (distanceUnitOfMeasurement.equals("sec")) { // TODO why are the
//            // values 1000 larger?
//            long hours = distance / 3600000L;
//            long minutes = distance % 3600000L / 60000L;
//            long seconds = distance % 60000L / 1000L;
//            long milliseconds = distance % 1000L;
//            return hours + "h " + minutes + "m " + seconds + "s " + milliseconds + "ms";
//        } else if (distanceUnitOfMeasurement.equals("km")) { // TODO why are the
//            // values 1000
//            // larger?
//            long km = distance / 1000L;
//            long meter = distance % 1000L;
//            return km + "km " + meter + "m";
//        } else if (distanceUnitOfMeasurement.equals("meter")) {
//            long km = distance / 1000L;
//            long meter = distance % 1000L;
//            return km + "km " + meter + "m";
//        } else {
//            return numberFormat.format(((double) distance) / 1000.0) + " " + distanceUnitOfMeasurement;
//        }
//    }

    @Override
    public Collection<? extends Object> getProblemFacts() {
        // TODO Auto-generated method stub
        return null;
    }
}

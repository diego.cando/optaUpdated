/*
 * Copyright 2015 Red Hat, Inc. and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.optaplanner.examples.vehiclerouting.domain.timewindowed.solver;

import org.apache.commons.lang3.ObjectUtils;
import org.optaplanner.core.impl.domain.variable.listener.VariableListener;
import org.optaplanner.core.impl.score.director.ScoreDirector;
import org.optaplanner.examples.vehiclerouting.domain.Vehicle;
import org.optaplanner.examples.vehiclerouting.domain.Standstill;
import org.optaplanner.examples.vehiclerouting.domain.timewindowed.TimeWindowedCustomer;

// TODO When this class is added only for TimeWindowedCustomer, use TimeWindowedCustomer instead of Customer
public class DummyDirtyUpdatingVariableListener implements VariableListener<Standstill> {

    public void beforeEntityAdded(ScoreDirector scoreDirector, Standstill vehicle) {
        // Do nothing
          //*System.out.println("abc1 ");
    }

    public void afterEntityAdded(ScoreDirector scoreDirector, Standstill vehicle) {
//        if (customer instanceof TimeWindowedCustomer) {
//            updateArrivalTime(scoreDirector, (TimeWindowedCustomer) customer);
//        }
   //*System.out.println("abc2 ");
    }

    public void beforeVariableChanged(ScoreDirector scoreDirector, Standstill vehicle) {
        // Do nothing
    }

    public void afterVariableChanged(ScoreDirector scoreDirector, Standstill vehicle) {
          //*System.out.println("abc3 ");
        
        if (vehicle instanceof Standstill) {
            //updateDummyDirty(scoreDirector, (Vehicle) vehicle);
        }
    }

    public void beforeEntityRemoved(ScoreDirector scoreDirector, Standstill vehicle) {
        // Do nothing
    }

    public void afterEntityRemoved(ScoreDirector scoreDirector, Standstill vehicle) {
        // Do nothing
    }

//    protected void updateArrivalTime(ScoreDirector scoreDirector, TimeWindowedCustomer sourceCustomer) {
//     
//    }

    protected void updateDummyDirty(ScoreDirector scoreDirector, Vehicle vehicle) {
          //*System.out.println("listerveh " + vehicle.getId());
        
        
    scoreDirector.beforeVariableChanged(vehicle, "dummyDirty");
   scoreDirector.afterVariableChanged(vehicle, "dummyDirty");  
    }
}

/*
 * Copyright 2012 Red Hat, Inc. and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.optaplanner.examples.vehiclerouting.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamInclude;

import java.util.HashMap;
import java.util.Map;

import org.optaplanner.core.api.domain.entity.PlanningEntity;
import org.optaplanner.core.api.domain.variable.AnchorShadowVariable;
import org.optaplanner.core.api.domain.variable.PlanningVariable;
import org.optaplanner.core.api.domain.variable.PlanningVariableGraphType;
import org.optaplanner.examples.common.domain.AbstractPersistable;
import org.optaplanner.examples.vehiclerouting.domain.location.Location;
import org.optaplanner.examples.vehiclerouting.domain.solver.DepotAngleCustomerDifficultyWeightFactory;
import org.optaplanner.examples.vehiclerouting.domain.timewindowed.TimeWindowedCustomer;

@PlanningEntity(difficultyWeightFactoryClass = DepotAngleCustomerDifficultyWeightFactory.class)
@XStreamAlias("VrpCustomer")
@XStreamInclude({TimeWindowedCustomer.class})
public class Customer extends AbstractPersistable implements Standstill {

    protected Location location;
    protected int demand;
    protected int demandh; // Ubicabus: demand handicapped
    protected int numCustSchool;

    protected boolean isFirst;
    //  private Long dummyDirty;
    //private Long dummyDirty;
    protected String ids;
    protected Map<Integer, Integer> students;
    protected Map<Integer, Integer> studentsh;
    
    protected boolean school;

    public boolean isSchool() {
        return school;
    }

    public void setSchool(boolean school) {
        this.school = school;
    }
    
    
    
    
    protected boolean hasStudents; 

    public boolean isHasStudents() {
        if (this.getVehicle() != null) {
            if (this.isSchool()){ //.getType().equals("SCHOOL")) {
                Customer cust = this.getVehicle().getNextCustomer();
                Map<Integer, Integer> students = new HashMap<Integer, Integer>();
                do {
                    for (Map.Entry entry : this.getStudents().entrySet()) {
                        if (cust.getStudents().get((Integer) entry.getKey()) != null) {
                            return true;
                        }
                    }
                    cust = cust.getNextCustomer();
                } while (cust.getId() != this.getId());

                return false;
            } else
                return true;
        }
        
        return false;
    }

    public void setHasStudents(boolean hasStudents) {
        this.hasStudents = hasStudents;
    }

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    public String getIds() {
        return ids;
    }

    public void setIds(String ids) {
        this.ids = ids;
    }

//          @CustomShadowVariable(variableListenerClass = DummyDirtyUpdatingVariableListener.class,
//            // Arguable, to adhere to API specs (although this works), nextCustomer should also be a source,valid
//            // because this shadow must be triggered after nextCustomer (but there is no need to be triggered by nextCustomer)
//          sources = {@CustomShadowVariable.Source(variableName = "previousStandstill")})
////        @PlanningVariable(valueRangeProviderRefs = { "vehicleRange" })
//    public Long getDummyDirty() {
//        return dummyDirty;
//    }
//
//    public void setDummyDirty(Long dummyDirty) {
//        this.dummyDirty = dummyDirty;
//    }
    public Map<Integer, Integer> getStudents() {
        // Update Students map
        //System.out.println("\n\nMensaje\n\n");
        if (this.students == null) {
            this.students = new HashMap<>();
            for (String schools : ids.split(";")) {
                String[] map = schools.split(",");
                if (map.length == 3) {// Check is a valid pair
                    if (this.students.get(Integer.parseInt(map[0].trim())) != null) {
                        // If the school is already in map
                        this.students.put(Integer.parseInt(map[0].trim()),
                                this.students.get(Integer.parseInt(map[0].trim())) + Integer.parseInt(map[1].trim()));
                    } else {
                        // New one
                        this.students.put(Integer.parseInt(map[0].trim()), Integer.parseInt(map[1].trim()));
                    }
                }
            }
        }
        // this.setStudents(this.students);
        return students;
    }

    public void setStudents(Map<Integer, Integer> students) {
        this.students = students;
    }

    public Map<Integer, Integer> getStudentsh() {
        //System.out.println("\n\nMensaje\n\n");
        // Update Students map
        if (this.studentsh == null) {
            this.studentsh = new HashMap<>();
            for (String schools : ids.split(";")) {
                String[] map = schools.split(",");
                if (map.length == 3) {// Check is a valid pair
                    if (this.studentsh.get(Integer.parseInt(map[0].trim())) != null) {
                        // If the school is already in map
                        this.studentsh.put(Integer.parseInt(map[0].trim()),
                                this.studentsh.get(Integer.parseInt(map[0].trim())) + Integer.parseInt(map[2].trim()));
                    } else {
                        // New one
                        this.studentsh.put(Integer.parseInt(map[0].trim()), Integer.parseInt(map[2].trim()));
                    }
                }
            }
        }
        // this.setStudents(this.students);
        return students;
    }

    public void setStudentsh(Map<Integer, Integer> studentsh) {
        this.studentsh = studentsh;
    }

    // diego agrega
    protected long distanceAccumulated;

    // Planning variables: changes during planning, between score calculations.
    protected Standstill previousStandstill;

    // Shadow variables
    protected Customer nextCustomer;
    protected Vehicle vehicle;

    // protected boolean required;
    protected String type; // DEPOT, STUDENTS, SCHOOL, BUSADDRESS
    
    // protected List<Vehicle> VehicleList;
    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public int getDemand() {
        return demand;
    }

    public void setDemand(int demand) {
        this.demand = demand;
    }

    public int getDemandh() {
        return demandh;
    }

    public void setDemandh(int demandh) {
        this.demandh = demandh;
    }

    public int getNumCustSchool() {
        return numCustSchool;
    }

    public void setNumCustSchool(int numCustSchool) {
        this.numCustSchool = numCustSchool;
    }

    public void setFirst(boolean isFirst) {
        this.isFirst = isFirst;
    }

    @PlanningVariable(valueRangeProviderRefs = {"vehicleRange",
        "customerRange"}, graphType = PlanningVariableGraphType.CHAINED)
    public Standstill getPreviousStandstill() {
        return previousStandstill;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getDistanceAccumulated() {
        return distanceAccumulated;
    }

    public void setDistanceAccumulated(long distanceAccumulated) {
        this.distanceAccumulated = distanceAccumulated;
    }

    public void setPreviousStandstill(Standstill previousStandstill) {
        this.previousStandstill = previousStandstill;
    }

    public Customer getNextCustomer() {
        return nextCustomer;
    }

    public void setNextCustomer(Customer nextCustomer) {
        this.nextCustomer = nextCustomer;
    }

    @AnchorShadowVariable(sourceVariableName = "previousStandstill")
    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public int numCustomersVehicle() {
        int numCust = 0;

        if (this.getVehicle() != null) {
            ////**System.out.println("bla");
            Customer cust = this.getVehicle().getNextCustomer();

            do {
                numCust += 1;

                cust = cust.getNextCustomer();
            } while (cust != null);
        }

        return numCust;
    }

    public Integer capacityBreak() {
        //System.out.println("F() capacityBreak, customer id: " + this.getId()+" vehicle customer list: "+this.getVehicle().whosInMe() + "");    

        int response = 0;
        int responseh = 0;
        int maxExceeded = 0;

        response = this.getVehicle().getCapacity();
        responseh = this.getVehicle().getCapacityh();

        Map<Integer, Integer> currentCapacity = new HashMap<Integer, Integer>();
        Map<Integer, Integer> currentCapacityHandicap = new HashMap<Integer, Integer>();
        // get first customer of route
        Customer cust = this.getVehicle().getNextCustomer();

        do {
            if (cust.isSchool()){ //.getType().equals("SCHOOL")) {
                // is school, increase actual capacity by the students exiting

                for (Map.Entry entry : cust.getStudents().entrySet()) {
                    if (currentCapacity.get((Integer) entry.getKey()) != null) {
                        response += currentCapacity.get((Integer) entry.getKey());
                        currentCapacity.put((Integer) entry.getKey(), 0);
                    }
                }

                for (Map.Entry entry : cust.getStudentsh().entrySet()) {
                    if (currentCapacityHandicap.get((Integer) entry.getKey()) != null) {
                        responseh += currentCapacityHandicap.get((Integer) entry.getKey());
                        currentCapacityHandicap.put((Integer) entry.getKey(), 0);
                    }
                }
            } else {
                // is student, reduce actual capacity by the students entering

                response -= cust.getDemand();
                responseh -= cust.getDemandh();

                for (Map.Entry entry : cust.getStudents().entrySet()) {
                    if (currentCapacity.get((Integer) entry.getKey()) == null) {
                        currentCapacity.put((Integer) entry.getKey(), (Integer) entry.getValue());
                    } else {
                        currentCapacity.put((Integer) entry.getKey(),
                                currentCapacity.get((Integer) entry.getKey()) + (Integer) entry.getValue());
                    }
                }

                for (Map.Entry entry : cust.getStudentsh().entrySet()) {
                    if (currentCapacityHandicap.get((Integer) entry.getKey()) == null) {
                        currentCapacityHandicap.put((Integer) entry.getKey(), (Integer) entry.getValue());
                    } else {
                        currentCapacityHandicap.put((Integer) entry.getKey(),
                                currentCapacityHandicap.get((Integer) entry.getKey()) + (Integer) entry.getValue());
                    }
                }
            }

            // if already over capacity
            if (response < 0 || responseh < 0) {
                if (response < maxExceeded) {
                    maxExceeded = response;
                }

                if (responseh < maxExceeded) {
                    maxExceeded = responseh;
                }
                //    //x
                //System.out.println("F() capacityBreak, customer id: " + this.getId()+" vehicle customer list: "+this.getVehicle().whosInMe() + " WRONG");
                //    return -1;
            }

            cust = cust.getNextCustomer();

        } while (cust != null);

        //x
        if (maxExceeded < 0) {
            System.out.println("F() capacityBreak, maxExceeded (" + maxExceeded + "), customer id: " + this.getId() + " vehicle customer list: " + this.getVehicle().whosInMe() + " WRONG");
            return maxExceeded;
        } else {
            System.out.println("F() capacityBreak, customer id: " + this.getId() + " vehicle customer list: " + this.getVehicle().whosInMe() + " CORRECT");
            return 0;
        }
    }

    public Integer studentBelongsDepot() {
        if (this.getVehicle() != null && !this.isSchool() /*this.getType().equals("STUDENTS")*/ && this.getVehicle().isHasSchoolsInRoute() == false) {
            //get id of depot/school
            Integer idsDepot = (int) (long) this.getVehicle().getDepot().getId();

            //dummy for because only 1 item since no cluster should have from multiple schools
            for (Map.Entry entry : this.getStudents().entrySet()) {
                //check if student corresponds to depot/school
                if (!idsDepot.equals((Integer) entry.getKey())) {
                    return 10000;
                }
            }
        }

        return 0;
    }

    public boolean mySchoolsBeforeMe() {
        ////**System.out.println("F() mySchoolsBeforeMe, customer id: " + this.getId()+" vehicle customer list: "+this.getVehicle().whosInMe());
        Map<Integer, Integer> schools = new HashMap<Integer, Integer>();

        // get first customer of route
        Customer cust = this.getVehicle().getNextCustomer();

        if (cust.getId() != this.getId()) {
            do {
                if (cust.isSchool()){ //.getType().equals("SCHOOL")) {
                    // is school, add school_ids to school_hash
                    for (Map.Entry entry : cust.getStudents().entrySet()) {
                        schools.put((Integer) entry.getKey(), 0);
                    }

                    // for each of my schools, check if school is before
                    for (Map.Entry entry : this.getStudents().entrySet()) {
                        if (schools.get((Integer) entry.getKey()) != null) // schools found, error
                        {
                            //**System.out.println("F() mySchoolsBeforeMe, customer id: " + this.getId()+" vehicle customer list: "+this.getVehicle().whosInMe() + " WRONG");
                            return true;
                        }
                    }
                }

                cust = cust.getNextCustomer();

            } while (cust.getId() != this.getId());
        }

        //x
        //**System.out.println("F() mySchoolsBeforeMe, customer id: " + this.getId()+" vehicle customer list: "+this.getVehicle().whosInMe() + " CORRECT");
        return false;
    }

    public Integer validMultiSchool() {
        if (this.getVehicle() != null && !this.isSchool()/*this.getType().equals("STUDENTS")*/ && this.getVehicle().isHasSchoolsInRoute() == true && this.getVehicle().isMultiSchool() == false) {
            // get first customer of route
            Customer cust = this.getVehicle().getNextCustomer();
            Map<Integer, Integer> schools = new HashMap<Integer, Integer>();

            do {
                if (!cust.isSchool()){ //cust.getType().equals("STUDENTS")) {
                    //dummy for because only 1 item since no cluster should have from multiple schools
                    for (Map.Entry entry : cust.getStudents().entrySet()) {
                        if (schools.get((Integer) entry.getKey()) != null) {
                            schools.put((Integer) entry.getKey(), schools.get((Integer) entry.getKey()) + 1);
                        } else {
                            schools.put((Integer) entry.getKey(), 1);
                        }
                    }
                }

                cust = cust.getNextCustomer();
            } while (cust != null);

            if (schools.size() > 1) {
                //check if this belongs to school with most students, else return error
                Integer response = 0;
                Integer schoolidthis = 0;
                for (Map.Entry entry : this.getStudents().entrySet()) {
                    response = schools.get((Integer) entry.getKey());
                    schoolidthis = (Integer) entry.getKey();
                }

                Integer max = 0;
                Integer schoolid = 0;
                for (Map.Entry entry : schools.entrySet()) {
                    if (max < (Integer) entry.getValue()) {
                        max = (Integer) entry.getValue();
                        schoolid = (Integer) entry.getKey();
                    }
                }

                if (response == max && schoolidthis == schoolid) {
                    return 0;
                } else {
                    return 10000;
                }
            } else {
                return 0;
            }
        }

        return 0;
    }

    public Integer isSchoolValid() {
        if (this.getVehicle() != null && this.isSchool() /*.getType().equals("SCHOOL")*/ && this.getVehicle().isHasSchoolsInRoute() == true) {
            Integer schoolBreak = 0;
            Integer customerBreak = 0;

            //get first customer of route
            Customer cust = this.getVehicle().getNextCustomer();

            //count schools equal to me
            do {
                if (cust.isSchool()){ //.getType().equals("SCHOOL")) {
                    //dummy for because only 1 item
                    for (Map.Entry entry : this.getStudents().entrySet()) {
                        if (cust.getStudents().get((Integer) entry.getKey()) != null && cust.getId() != this.getId()) {
                            schoolBreak += 1;
                        }
                    }
                }

                cust = cust.getNextCustomer();

            } while (cust != null);

            //no students should be after me
            cust = this.getNextCustomer();
            if (cust != null) {
                do {
                    if (!cust.isSchool()){ //cust.getType().equals("STUDENTS")) {
                        customerBreak += 1;
                    }

                    cust = cust.getNextCustomer();

                } while (cust != null);
            }

            return (customerBreak * 1000) + (schoolBreak * 10000);
        }

        return 0;
    }

    public boolean isRouteValid(int ac) {//int ac) {

        // Map<Integer, Integer> students;
        Customer cust = this.getNextCustomer();
        if (cust != null) {
            Map<Integer, Integer> schools = new HashMap<Integer, Integer>();
            // students = new HashMap<Integer, Integer>();
//            cust = this.getNextCustomer();
            //Integer response = 0;

            //add my school_ids to school_hash
            for (Map.Entry entry : this.getStudents().entrySet()) {
                schools.put((Integer) entry.getKey(), 0);
            }

            do {
                ////**System.out.println("*****Evaluating:("+cust.getId()+") ids("+cust.getIds()+") students("+students.size()+") schools("+schools.size()+")**********");
                if (cust.isSchool()){ //.getType().equals("SCHOOL")) {

                    // is school, add school_ids to school_hash
                    for (Map.Entry entry : cust.getStudents().entrySet()) {
                        if (schools.get((Integer) entry.getKey()) != null) {
                            schools.remove((Integer) entry.getKey());
                        }
                    }

                    if (schools.size() == 0) {
                        //**System.out.println("F() isRouteValid, customer id: " + this.getId()+" vehicle id:"+ this.getVehicle().getId()+" vehicle customer list: "+this.getVehicle().whosInMe() + " CORRECT");
                        return true;

                    }
                }

                cust = cust.getNextCustomer();

            } while (cust != null);
            //response = students.size() - schools.size();

            //**System.out.println("F() isRouteValid, customer id: " + this.getId()+" vehicle id:"+ this.getVehicle().getId()+" vehicle customer list: "+this.getVehicle().whosInMe() + " WRONG");
            return false;
        } else {
            //**System.out.println("F() isRouteValid, customer id: " + this.getId()+" vehicle id:"+ this.getVehicle().getId()+" vehicle customer list: "+this.getVehicle().whosInMe() + " WRONG2");
            return false;
        }
    }

    public int getStudentAfterSchool() {
        int response = 0;

        Map<Integer, Integer> students = new HashMap<Integer, Integer>();

        // get first customer of route
        Customer cust = this.getVehicle().getNextCustomer();
        if (cust != null) {
            do {
                if (cust.isSchool()) //.getType().equals("SCHOOL")) // is school, remove school_ids from student_hash
                {
                    for (Map.Entry entry : cust.getStudents().entrySet()) {
                        students.remove((Integer) entry.getKey());
                    }
                } else // is student, add school_ids to student_hash
                {
                    for (Map.Entry entry : cust.getStudents().entrySet()) {
                        students.put((Integer) entry.getKey(), 0);
                    }
                }

                cust = cust.getNextCustomer();

            } while (cust != null);

            response = students.size();
        }

        return response;
    }

    public long getTypeWeight() {

        if (!school){ //type.equals("STUDENTS")) {
            ////**System.out.println("UNO id: " + id + " this type:" + this.getType() + " type:" + type);
            return 1;
        } else {
            ////**System.out.println("CERO id: " + id + " this type:" + this.getType() + " type:" + type);
            return 0;
        }
    }
    // ************************************************************************
    // Complex methods
    // ************************************************************************

    /**
     * @return a positive number, the distance multiplied by 1000 to avoid
     * floating point arithmetic rounding errors
     */
    public long getDistanceFromPreviousStandstill() {

        if (previousStandstill == null) {
            // return 0L;
            throw new IllegalStateException("This method must not be called when the previousStandstill ("
                    + previousStandstill + ") is not initialized yet.");
        }

        return getDistanceFrom(previousStandstill); 
    }


    /**
     * @param standstill never null
     * @return a positive number, the distance multiplied by 1000 to avoid
     * floating point arithmetic rounding errors
     */
    public long getDistanceFrom(Standstill standstill) {
        return standstill.getLocation().getDistanceTo(location);
    }

    /**
     * @param standstill never null
     * @return a positive number, the distance multiplied by 1000 to avoid
     * floating point arithmetic rounding errors
     */
    public long getDistanceTo(Standstill standstill) {
        return location.getDistanceTo(standstill.getLocation());
    }

    @Override
    public String toString() {
        if (location.getName() == null) {
            return super.toString();
        }
        return location.getName();
    }

}
